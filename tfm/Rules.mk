TFM_FILES := $(patsubst %,$(d)/%.tfm,$(VARIANTS))

CLEAN += $(d)/*.tfm $(d)/*.log $(d)/*.600gf
INSTALLED += $(TFM_FILES)

install.sh: $(d)/install-parts.sh
CLEAN += $(d)/install-parts.sh
$(d)/install-parts.sh: $(d)/Rules.mk
	$(ECHO) [SCRIPT] $@
	$(QUIET) echo install -d '$$1/fonts/tfm/public/cmll' >> $@
	$(QUIET) echo install -t '$$1/fonts/tfm/public/cmll' $(TFM_FILES) >> $@

$(d)/%.tfm: mf/%.mf mf/llsymbols.mf
	$(ECHO) [TFM] $@
	$(QUIET) cd $(dir $@) ; $(MFENV) mf "\mode:=ljfour; mag:=1; batchmode; input $*" >/dev/null
