#!/bin/sh

FONT=""
PREFIX=""

verbosely ()
{
	echo "$*" >& 2
	"$@"
}

for ARG; do
	case $ARG in
	-v) PREFIX=verbosely ;;
	-n) PREFIX=echo ;;
	-*)
		echo "unknown option $ARG" >&2
		exit 1 ;;
	*)
		if [ -n "$FONT" ]; then
			echo "too many arguments" >&2
			exit 1
		fi
		FONT="$ARG"
	esac
done

case $FONT in
cmllss*)
	FAMILY="CMLL Sans"
	BASE="cmllss"
	;;
cmll*)
	FAMILY="CMLL"
	BASE="cmll"
	;;
eull*)
	FAMILY="EuLL"
	BASE="eull"
	;;
*)
	echo "invalid font name"
	exit 1
esac

N="${FONT#$BASE}"

case $N in
bx*)
	WEIGHT="Bold"
	FORM="Bold Extended"
	SIZE=${N#bx}
	;;
*)
	WEIGHT="Regular"
	FORM="Regular"
	SIZE=${N#r}
esac

$PREFIX mftrace \
-D"FullName=$FAMILY $FORM ${SIZE}pt" \
-D"FamilyName=$FAMILY" \
-D"Weight=$WEIGHT" \
-D"FontName=$FONT" \
--formats=PFB --simplify $FONT > $FONT.log 2>&1

if [ $? -ne 0 ]; then
	cat $FONT.log >&2
	exit 1
fi
