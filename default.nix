with import <nixpkgs> {};
let
  mftrace =
    stdenv.mkDerivation {
      name = "mftrace-1.2.20";
      src = fetchurl {
        url = http://lilypond.org/downloads/sources/mftrace/mftrace-1.2.20.tar.gz;
        sha256 = "0vszxacv3a54s3f2bxafl9gazmk859ir58sv363c0s578ncplsv2";
      };
      buildInputs = [
        fontforge
        potrace
        python3
        t1utils
        # texlive.kpathsea
        # texlive.metafont
      ];
      preConfigure = ''
        sed -i 's/ gf2pbm.1$//' GNUmakefile.in
        '';
      meta = {
        description = "Trace bitmaps in scalable fonts";
        homepage = http://lilypond.org/mftrace/;
      };
    };
in
  buildEnv {
    name = "cmll";
    paths = [ ];
    buildInputs = [ mftrace potrace fontforge t1utils ];
  }
