SOURCES := $(patsubst %,$(d)/%.mf,$(CM_VARIANTS))
#SOURCES += $(patsubst %,$(d)/%.mf,$(EU_VARIANTS))

CLEAN += $(SOURCES)
INSTALLED += $(SOURCES)

install.sh: $(d)/install-parts.sh
CLEAN += $(d)/install-parts.sh
$(d)/install-parts.sh: Makefile $(d)/Rules.mk
	$(ECHO) [SCRIPT] $@
	$(QUIET) echo install -d '$$1/fonts/source/public/cmll' > $@
	$(QUIET) echo install -t '$$1/fonts/source/public/cmll' $(SOURCES) >> $@

CM_PREFIX = $(dir $(shell kpsewhich cmr10.mf))
#EU_PREFIX = $(dir $(shell kpsewhich eufm10.mf))

CM_SCRIPT = sed -e "/^%/d" \
	-e "/font_identifier/ s/CM/CMLL/" \
	-e "/generate/ s/roman/llsymbols/"

$(d)/cmll%.mf: $(CM_PREFIX)cm%.mf $(d)/Rules.mk
	$(ECHO) [SOURCE] $@
	$(QUIET) $(CM_SCRIPT) $< > $@
