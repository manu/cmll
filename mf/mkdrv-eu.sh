#!/bin/sh
#
# This script is used to generate driver files for EULL from the drivers of
# the standard AMS Euler fonts.
#
# Emmanuel Beffara, 2004, public domain.

# Where the drivers of AMS Euler are found:

if which kpsewhich > /dev/null; then
	SRCDIR=$(dirname $(kpsewhich eufm10.mf))
	CMSRCDIR=$(dirname $(kpsewhich cmr10.mf))
else
	BASEDIR="/usr/share/texmf/fonts/source"
	SRCDIR="$BASEDIR/ams/euler"
	CMSRCDIR="$BASEDIR/public/cm"
fi

# Which series are to be built:

SERIES="fm=r fb=bx"
SIZES="5 6 7 8 9 10"

# Generate all drivers.

for S in $SERIES; do
	S1=${S%=*}
	S2=${S#*=}
	for Sz in $SIZES; do
		echo eull$S2$Sz.mf
		ed -s $CMSRCDIR/cm$S2$Sz.mf <<EOF
1,/^u#/-1 d
1i
def cmsetup =
.
/^generate/,\$ d
\$a
enddef;
.
w temp
EOF
		ed -s $SRCDIR/eu$S1$Sz.mf <<EOF
1,/^[^%]/-1 d
1i
%% EULL driver file, series $S2, size $Sz
%% this file was generated from eu$S1$Sz.mf

.
/font_identifier/ s/EUF\([A-Z]*\)[^"]*/EULL\1/
/input/ s/eu${S1}ch/lleusym/
/input/-1 r temp
w eull$S2$Sz.mf
EOF
	done
done

rm temp
