# main Makefile for CMLL
# (c) 2002--2009 Emmanuel Beffara, LPPL
#
#----------------------------------------------------------
#
# Modify the following variables to install the files in the appropriate
# directories for your system.
#
# The base of the TeX hierarchy for installation:

TEXMF = /usr/share/texmf

# The variants that will be included:

CM_VARIANTS := \
	cmllr5 cmllr6 cmllr7 cmllr8 cmllr9 cmllr10 cmllr12 cmllr17 \
	cmllbx5 cmllbx6 cmllbx7 cmllbx8 cmllbx9 cmllbx10 cmllbx12 \
	cmllss8 cmllss9 cmllss10 cmllss12 cmllss17 \
	cmllssbx10

EU_VARIANTS := \
	eullr5 eullr6 eullr7 eullr8 eullr9 eullr10 \
	eullbx5 eullbx6 eullbx7 eullbx8 eullbx9 eullbx10

# Build and install the Type1 version ? (building requires mftrace)

TYPE1 = true

# Here ends the customization part.
#
#----------------------------------------------------------

ECHO = @echo
QUIET = @

.PHONY: all clean install

all:

clean:
	rm -rf $(CLEAN)

VARIANTS := $(CM_VARIANTS) $(EU_VARIANTS)

MFENV := env \
 MFINPUTS=$(abspath mf):$(abspath tfm):$$MFINPUTS
PDFLATEX := $(MFENV) \
 TEXINPUTS=$(abspath latex):$$TEXINPUTS \
 T1FONTS=$(abspath type1):$$T1FONTS \
 pdflatex -interaction=batchmode

install.sh:
	$(ECHO) [SCRIPT] $@
	$(QUIET) echo install -d '$$1/doc/fonts/cmll' > $@
	$(QUIET) echo install -t '$$1/doc/fonts/cmll' README cmll.pdf >> $@
	$(QUIET) echo install -d '$$1/fonts/map/dvips' > $@
	$(QUIET) echo install -t '$$1/fonts/map/dvips' cmll.map >> $@
	$(QUIET) cat $^ >> $@

cmll.map: Makefile
	$(ECHO) [GEN] $@
	$(QUIET) for V in $(VARIANTS); do echo "$$V $$V <$$V.pfb" ; done > $@

pdftex.map: cmll.map
	$(ECHO) [GEN] $@
	$(QUIET) rm -f $@
	$(QUIET) grep -v '^\(cm\|eu\)ll' $$(kpsewhich pdftex.map) > $@
	$(QUIET) cat $< >> $@

cmll.pdf: latex/cmll.dtx latex/cmll.sty latex/ucmllr.fd tfm/cmllr10.tfm type1/cmllr10.pfb pdftex.map
	$(ECHO) [PDFLATEX] $<
	$(QUIET) $(PDFLATEX) $< >/dev/null
	$(QUIET) $(PDFLATEX) $< >/dev/null

CLEAN := cmll.tds.zip cmll.zip install.sh cmll.map cmll.log cmll.aux cmll.pdf pdftex.map
INSTALLED := README cmll.map cmll.pdf

d := latex
include $(d)/Rules.mk
d := mf
include $(d)/Rules.mk
d := tfm
include $(d)/Rules.mk
d := type1
include $(d)/Rules.mk

all: $(INSTALLED)

install: $(INSTALLED) install.sh
	sh install.sh $(TEXMF)

cmll.tds.zip: $(INSTALLED) install.sh
	$(ECHO) [ZIP] $@
	$(QUIET) rm -rf tds cmll.tds.zip
	$(QUIET) sh install.sh tds
	$(QUIET) cd tds ; zip -qr ../cmll.tds.zip *
	$(QUIET) rm -rf tds

cmll.zip: $(INSTALLED) cmll.tds.zip
	$(ECHO) [ZIP] $@
	$(QUIET) rm -f $@
	$(QUIET) test -e cmll || ln -s . cmll
	$(QUIET) zip -qr $@ $(addprefix cmll/,$(INSTALLED)) cmll.tds.zip
	$(QUIET) rm cmll

cmll.tar.gz: $(INSTALLED) cmll.tds.zip
	$(ECHO) [TAR] $@
	$(QUIET) rm -rf cmll

#dist: dist-tree
#	tar zcf cmll.tar.gz cmll
#	rm -rf cmll

#ctan: dist-tree cmll.tds.zip
#	rm -f cmll.zip
#	zip -qr cmll.zip cmll cmll.tds.zip
#	rm -rf cmll cmll.tds.zip
