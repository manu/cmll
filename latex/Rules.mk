LATEX_SOURCES := $(d)/cmll.ins $(d)/cmll.dtx
LATEX_GENERATED := $(shell sed -n 's/\\generateFile{\([^{}]*\)}.*/\1/p' $(d)/cmll.ins)
LATEX_FILES := $(addprefix $(d)/,$(LATEX_GENERATED))

CLEAN += $(LATEX_FILES) $(d)/cmll.ins.stamp
INSTALLED += $(LATEX_FILES) $(LATEX_SOURCES)

install.sh: $(d)/install-parts.sh
CLEAN += $(d)/install-parts.sh
$(d)/install-parts.sh: $(d)/Rules.mk
	$(ECHO) [SCRIPT] $@
	$(QUIET) echo install -d '$$1/tex/latex/cmll' > $@
	$(QUIET) echo install -t '$$1/tex/latex/cmll' $(LATEX_FILES) >> $@
	$(QUIET) echo install -d '$$1/source/latex/cmll' >> $@
	$(QUIET) echo install -t '$$1/source/latex/cmll' $(LATEX_SOURCES) >> $@

# The trick with the stamp target is to make sure that the ins file is
# compiled at most once, even with parallel make, otherwise make understands
# multiple targets as multiple independent rules.
$(LATEX_FILES): $(d)/cmll.ins.stamp
	$(QUIET) touch -c $@
$(d)/cmll.ins.stamp: $(d)/cmll.ins $(d)/cmll.dtx
	$(ECHO) [LATEX] $<
	$(QUIET) cd $(dir $<) ; latex -interaction=batchmode cmll.ins >/dev/null
	$(QUIET) touch $@
