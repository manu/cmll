PFB_FILES := $(patsubst %,$(d)/%.pfb,$(VARIANTS))

CLEAN += $(d)/*.pfb $(d)/*.log
INSTALLED += $(PFB_FILES)

install.sh: $(d)/install-parts.sh
CLEAN += $(d)/install-parts.sh
$(d)/install-parts.sh: $(d)/Rules.mk
	$(ECHO) [SCRIPT] $@
	$(QUIET) echo install -d '$$1/fonts/type1/public/cmll' >> $@
	$(QUIET) echo install -t '$$1/fonts/type1/public/cmll' $(PFB_FILES) >> $@

$(d)/%.pfb: mf/%.mf mf/llsymbols.mf tfm/%.tfm $(d)/convert.sh
	$(ECHO) [MFTRACE] $@
	$(QUIET) cd $(dir $@) ; $(MFENV) ./convert.sh $*
